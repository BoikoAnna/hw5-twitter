import Card from "./Card.js";

const API = "https://ajax.test-danit.com/api/json/";

// Функція sendRequest є асинхронною функцією, приймає три параметри - URL-адресу, метод GET та конфігурацію
// використовує функцію fetch для виконання запиту до API і повертає результат або об'єкт помилки,
// якщо запит не був успішним.

const sendRequest = async (url, method = "GET", config) => {
  return await fetch(url, {
    method,
    body: config,
  }).then((response) => {
    if (response.ok) {
      if (method === "DELETE") {
        return response;
      } else {
        return response.json();
      }
    } else {
      return new Error("Somesing wrong...!");
    }
  });
};

// Функції getAuthors, getPosts, deletePost, sendPost та putPost є функціями-обгортками, які використовують
// sendRequest для виконання запитів до API. getAuthors та getPosts виконують запит GET до відповідних URL-адрес, тоді як
// deletePost, sendPost та putPost виконують запит DELETE, POST та PUT, відповідно, залежно від параметру method.
const getAuthors = () => sendRequest(`${API}users`);
const getPosts = () => sendRequest(`${API}posts`);
const deletePost = (id) => sendRequest(`${API}posts/${id}`, "DELETE");
const sendPost = (config) =>
  sendRequest(`${API}posts`, "POST", JSON.stringify(config));
const putPost = (id, config) =>
  sendRequest(`${API}posts/${id}`, "PUT", JSON.stringify(config));

// Функція createPostsArr є асинхронною функцією, яка використовує Promise.all для одночасного виконання запитів до API з
// getAuthors та getPosts. Після отримання результатів, вона використовує forEach для перебору авторів та дописів та створює
// новий об'єкт Card для кожного допису, який належить автору. Об'єкти Card додаються до масиву cardsArr, який повертається
// з функції.

const createPostsArr = async () => {
  const cardsArr = [];

  await Promise.all([getAuthors(), getPosts()]).then((data) => {
    const [authorsArr, postsArr] = data;

    authorsArr.forEach((author) => {
      postsArr.forEach((post) => {
        if (author.id === post.userId) {
          const card = new Card({
            title: post.title,
            text: post.body,
            author: author.name,
            email: author.email,
            cardID: post.id,
            userID: author.id,
          });
          cardsArr.push(card);
        }
      });
    });
  });
  return cardsArr;
};

// ескпортуємо createPostsArr, deletePost, sendPost та putPost, щоб могли бути використані в інших модулях
export { createPostsArr, deletePost, sendPost, putPost };
