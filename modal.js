import { sendPost } from "./services.js";
import Card from "./Card.js";

// Клас Modal створює модальне вікно, де користувач може створювати новий пост. 
// При створенні екземпляра класу, конструктор отримує об'єкт з властивістю headerTitle,
//  яка визначає заголовок модального вікна.

export default class Modal {
  constructor({ headerTitle }) {
    this.headerTitle = headerTitle;
  }

// Метод closeModal() служить для закриття модального вікна, він вішає обробник події "click" на модальне вікно.
//  Якщо користувач клікає на модальному вікні, яке має клас modal, або на елементі з класом close-btn, то вікно закривається.
  closeModal() {
    this.modal.addEventListener("click", (e) => {
      if (
        e.target.classList.contains("modal") ||
        e.target.classList.contains("close-btn")
      ) {
        this.modal.remove();
        document.body.classList.remove("modal-open");
      }
    });
  }

// Метод createNewPost() служить для створення нового поста. Він вішає обробник події "submit" на форму модального вікна. 
// При надсиланні форми, він отримує значення поля title і body, перевіряє, що поля не пусті і створює новий об'єкт Card,
// який відображає новий пост на сторінці. Він також викликає функцію sendPost для надсилання нового поста на сервер.

  createNewPost() {
    this.modal.querySelector(".modal__form").addEventListener("submit", (e) => {
      e.preventDefault();

      const title = this.modal.querySelector("input").value;
      const body = this.modal.querySelector("textarea").value;
      if (!title || !body) {
        alert("Fill out the form!");
        return;
      }

      const userId = 1;
      const author = "Leanne Graham";
      const email = "sincere@april.biz";
      const allCards = [...document.querySelectorAll(".card")];
      const cardID =
        Math.max(...allCards.map((el) => el.getAttribute("data-post-id"))) + 1;

      const card = new Card({
        title,
        text: body,
        author,
        email,
        cardID,
      });

      document
        .querySelector(".cards-list")
        .insertAdjacentElement("afterbegin", card.render());

      sendPost({
        cardID,
        userId,
        title,
        body,
      }).then(console.log);

      this.modal.remove();
      document.body.classList.remove("modal-open");
    });
  }

// Метод render() створює HTML структуру модального вікна, вішає обробники подій на кнопки і форму і повертає створений HTML
//  елемент

  render() {
    document.body.classList.add("modal-open");

    this.modal = document.createElement("div");
    this.modal.classList.add("modal");
    this.modal.innerHTML = `
			<div class="modal__content">
				<h2 class="modal__title">${this.headerTitle}</h2>
				<form class="modal__form">
					<label class="modal__label">Title: <input type="text" /></label>
					<label class="modal__label">Text: <textarea></textarea></label>
					<div class="modal__btns-wrapper">
						<button class="btn" type="submit">Add post</button>
						<button class="btn close-btn" type="button">Cansel</button>
					</div>
				</form>
			</div>
		`;

    this.createNewPost();
    this.closeModal();

    return this.modal;
  }
}
